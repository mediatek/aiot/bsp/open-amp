/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <fcntl.h>

#include <rich-iot/log.h>

#define TAG "libapu(sysfs.c)"

static char *_get_rproc_subsys(const char *base)
{
	DIR *d;
	char *path;
	const char *subsys = "/remoteproc";

	path = malloc(strlen(base) + strlen(subsys) + 1);
	if (!path) {
		errno = ENOMEM;
		return NULL;
	}
	strcpy(path, base);
	strcat(path, subsys);

	d = opendir(path);
	if (!d) {
		free(path);
		return NULL;
	} else {
		closedir(d);
	}

	return path;
}

static char *get_rproc_subsys_path(void)
{
	char *path;

	path = _get_rproc_subsys("/sys/subsystem");
	if (path)
		return path;
	else if (errno != ENOENT)
		return NULL;

	path = _get_rproc_subsys("/sys/bus");
	if (path)
		return path;
	else if (errno != ENOENT)
		return NULL;

	path = _get_rproc_subsys("/sys/class");
	if (path)
		return path;
	else if (errno != ENOENT)
		return NULL;

	return NULL;
}

static char *_get_rproc_device_path(int rproc_id, int log)
{
	DIR *d;
	char *path;
	char *device_path;
	char device_name[32];

	path = get_rproc_subsys_path();
	if (!path) {
		if (log)
			LOGE(TAG, "Failed to find remoteproc subsystem: %s\n",
				strerror(errno));
		return NULL;
	}

	snprintf(device_name, 32, "/remoteproc%d", rproc_id);
	device_path = malloc(strlen(path) + strlen(device_name) + 1);
	if (!device_path) {
		free(path);
		errno = ENOMEM;
		return NULL;
	}
	strcpy(device_path, path);
	strcat(device_path, device_name);
	free(path);

	d = opendir(device_path);
	if (!d) {
		if (log)
			LOGE(TAG, "Invalid device path %s: %s\n", device_path,
				strerror(errno));
		free(device_path);
		return NULL;
	} else {
		closedir(d);
	}

	return device_path;
}

char *get_rproc_device_path(int rproc_id)
{
	return _get_rproc_device_path(rproc_id, 1);
}

char *get_rproc_device_path_quiet(int rproc_id)
{
	return _get_rproc_device_path(rproc_id, 0);
}

static char *get_rproc_device_attr_path(char *device_path, char *attr)
{
	FILE *file;
	char *attr_path;

	attr_path = malloc(strlen(device_path) + strlen(attr) + 2);
	if (!attr_path) {
		errno = ENOMEM;
		return NULL;
	}
	sprintf(attr_path, "%s/%s", device_path, attr);

	file = fopen(attr_path, "r");
	if (file) {
		fclose(file);
	} else {
		LOGE(TAG, "Invalid attribute path %s: %s\n", attr_path,
			strerror(errno));
		free(attr_path);
		return NULL;
	}

	return attr_path;
}

static int sysfs_attr_write(char *attr_path, char *value)
{
	int fd;
	int ret;

	fd = open(attr_path, O_WRONLY);
	if (fd == -1)
		return -1;

	ret = write(fd, value, strlen(value));

	close(fd);

	return ret == -1 ? -1 : 0;
}

static int sysfs_attr_read(char *attr_path, char *value, int len)
{
	int fd;
	int ret;

	fd = open(attr_path, O_RDONLY);
	if (fd == -1)
		return -1;

	ret = read(fd, value, len);

	close(fd);

	return ret;
}

int rproc_device_attr_write_wait(char *device_path, char *attr, char *value,
				 char *expected, struct timeval *timeout)
{
	struct timeval start_time, end_time;
	int buffer_len = expected ? strlen(expected) : 0;
	char buffer[expected ? buffer_len + 1 : 0];
	char *attr_path;
	int ret;

	attr_path = get_rproc_device_attr_path(device_path, attr);
	if (!attr_path)
		return -1;

	ret = sysfs_attr_write(attr_path, value);
	if (ret)
		goto free_attr_path;

	gettimeofday(&start_time, NULL);
	timeradd(&start_time, timeout, &end_time);
	while (timercmp(&start_time, &end_time, <=)) {
		int len;

		len = sysfs_attr_read(attr_path, buffer, buffer_len);
		if (len >= 0 && strncmp(expected, buffer, buffer_len) == 0)
			goto free_attr_path;
		gettimeofday(&start_time, NULL);
	}

	LOGE(TAG, "Invalid value read back from %s: read %s, expected %s\n",
		attr_path, buffer, expected);
	ret = -1;
	errno = ETIMEDOUT;

free_attr_path:
	free(attr_path);

	return ret == -1 ? -1 : 0;
}

int rproc_device_attr_read_cmp(char *device_path, char *attr, char *expected)
{
	int buffer_len = strlen(expected);
	char buffer[buffer_len + 1];
	char *attr_path;
	int len;
	int ret;

	attr_path = get_rproc_device_attr_path(device_path, attr);
	if (!attr_path)
		return -1;

	len = sysfs_attr_read(attr_path, buffer, buffer_len);
	if (len >= 0 && strncmp(expected, buffer, buffer_len) == 0) {
		ret = 0;
	} else {
		LOGE(TAG, "Invalid value read from %s: read %s, expected %s\n",
			attr_path, buffer, expected);
		ret = -1;
		errno = EINVAL;
	}
	free(attr_path);

	return ret;
}
