/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <uapi/dma-buf.h>
#include <uapi/mtk_apu.h>

#include <rich-iot/apu-host.h>
#include <rich-iot/log.h>
#include <rich-iot/memory.h>

#include <apu_drmif.h>
#include <errno.h>
#define TAG "libapu(memory.c)"

struct apu_buffer *apu_alloc_buffer(struct apu_device *dev, size_t size)
{
	struct apu_buffer *buffer;

	buffer = malloc(sizeof(*buffer));
	if (!buffer)
		return NULL;

	buffer->bo = apu_cached_bo_new(dev->drm, size, 0);
	if (!buffer->bo) {
		LOGE(TAG, "Failed to allocate apu bo\n");
		free(buffer);
		return NULL;
	}
	buffer->fd = apu_bo_dmabuf(buffer->bo);
	if (buffer->fd < 0) {
		LOGE(TAG, "Failed to dma_buf handle\n");
		apu_bo_del(buffer->bo);
		free(buffer);
		return NULL;
	}
	buffer->size = size;
	buffer->data_size = size;
	buffer->mmap_refcount = 0;
	buffer->sync_refcount = 0;
	buffer->iommu_refcount = 0;
	buffer->hostptr = NULL;
	pthread_mutex_init(&buffer->lock, NULL);

	return buffer;
}

struct apu_buffer *apu_cached_alloc_buffer(struct apu_device *dev, size_t size)
{
	struct apu_buffer *buffer;

	buffer = malloc(sizeof(*buffer));
	if (!buffer)
		return NULL;

	buffer->bo = apu_cached_bo_new(dev->drm, size, 0);
	if (!buffer->bo) {
		LOGE(TAG, "Failed to allocate apu bo\n");
		free(buffer);
		return NULL;
	}
	buffer->fd = apu_bo_dmabuf(buffer->bo);
	if (buffer->fd < 0) {
		LOGE(TAG, "Failed to dma_buf handle\n");
		apu_bo_del(buffer->bo);
		free(buffer);
		return NULL;
	}
	buffer->size = size;
	buffer->data_size = size;
	buffer->mmap_refcount = 0;
	buffer->sync_refcount = 0;
	buffer->iommu_refcount = 0;
	buffer->hostptr = NULL;
	pthread_mutex_init(&buffer->lock, NULL);

	return buffer;
}

struct apu_buffer *apu_alloc_user_buffer(struct apu_device *dev,
		void *hostptr, size_t size)
{
	struct apu_buffer *buffer;

	buffer = malloc(sizeof(*buffer));
	if (!buffer)
		return NULL;

	buffer->bo = apu_bo_user_new(dev->drm, hostptr, size, 0);
	if (!buffer->bo) {
		LOGE(TAG, "Failed to allocate apu bo\n");
		free(buffer);
		return NULL;
	}
	buffer->fd = apu_bo_dmabuf(buffer->bo);
	if (buffer->fd < 0) {
		LOGE(TAG, "Failed to dma_buf handle\n");
		apu_bo_del(buffer->bo);
		free(buffer);
		return NULL;
	}
	buffer->size = size;
	buffer->data_size = size;
	buffer->mmap_refcount = 0;
	buffer->sync_refcount = 0;
	buffer->iommu_refcount = 0;
	buffer->hostptr = hostptr;
	buffer->ptr = hostptr;
	pthread_mutex_init(&buffer->lock, NULL);

	return buffer;
}

void apu_free_buffer(struct apu_buffer *buffer)
{
	close(buffer->fd);
	apu_bo_del(buffer->bo);
	free(buffer);
}

void *apu_map_buffer(struct apu_buffer *buffer)
{
	if (buffer->hostptr)
		return buffer->hostptr;

	pthread_mutex_lock(&buffer->lock);
	if (buffer->mmap_refcount++ == 0) {
		buffer->ptr = mmap(NULL, buffer->size, PROT_READ | PROT_WRITE,
				   MAP_SHARED, buffer->fd, 0);
		if (buffer->ptr == MAP_FAILED) {
			pthread_mutex_unlock(&buffer->lock);
			return NULL;
		}
	}
	pthread_mutex_unlock(&buffer->lock);
	return buffer->ptr;
}

int apu_unmap_buffer(struct apu_buffer *buffer)
{
	if (buffer->hostptr)
		return 0;

	pthread_mutex_lock(&buffer->lock);
	if (buffer->mmap_refcount-- == 1) {
		int ret;

		ret = munmap(buffer->ptr, buffer->size);
		if (!ret)
			buffer->ptr = NULL;
		pthread_mutex_unlock(&buffer->lock);

		return ret;
	}
	pthread_mutex_unlock(&buffer->lock);

	return 0;
}

int apu_get_buffer_access_dir(struct apu_buffer *buffer, int dir)
{
	if (buffer->hostptr)
		return 0;

	pthread_mutex_lock(&buffer->lock);
	if (buffer->sync_refcount++ == 0) {
		struct dma_buf_sync sync_start = { 0 };
		int ret;

		sync_start.flags = DMA_BUF_SYNC_START | dir;
		ret = ioctl(buffer->fd, DMA_BUF_IOCTL_SYNC, &sync_start);
		pthread_mutex_unlock(&buffer->lock);

		return ret;
	}
	pthread_mutex_unlock(&buffer->lock);

	return 0;
}

int apu_get_buffer_access(struct apu_buffer *buffer)
{
	if (buffer->hostptr)
		return 0;
	return apu_get_buffer_access_dir(buffer, DMA_BUF_SYNC_RW);
}

int apu_put_buffer_access_dir(struct apu_buffer *buffer, int dir)
{
	pthread_mutex_lock(&buffer->lock);
	if (buffer->sync_refcount-- == 1) {
		struct dma_buf_sync sync_start = { 0 };
		int ret;

		sync_start.flags = DMA_BUF_SYNC_END | dir;
		ret = ioctl(buffer->fd, DMA_BUF_IOCTL_SYNC, &sync_start);
		pthread_mutex_unlock(&buffer->lock);

		return ret;
	}
	pthread_mutex_unlock(&buffer->lock);

	return 0;
}

int apu_put_buffer_access(struct apu_buffer *buffer)
{
	return apu_put_buffer_access_dir(buffer, DMA_BUF_SYNC_RW);
}

void *apu_get_buffer_dir(struct apu_buffer *buffer, int dir)
{
	void *ptr;

	ptr = apu_map_buffer(buffer);
	if (!ptr)
		return NULL;

	buffer->direction = dir;
	if (apu_get_buffer_access_dir(buffer, dir)) {
		apu_unmap_buffer(buffer);
		return NULL;
	}

	return ptr;
}

void *apu_get_buffer(struct apu_buffer *buffer)
{
	return apu_get_buffer_dir(buffer, DMA_BUF_SYNC_RW);
}

int apu_put_buffer(struct apu_buffer *buffer)
{
	int ret;

	ret = apu_put_buffer_access_dir(buffer, buffer->direction);
	if (ret)
		return ret;

	ret = apu_unmap_buffer(buffer);
	if (ret)
		return ret;

	return 0;
}

uint32_t apu_iommu_map_buffer(struct apu_device *dev,
			      struct apu_buffer *buffer)
{
	pthread_mutex_lock(&buffer->lock);
	if (buffer->iommu_refcount++ == 0) {
		struct apu_bo *bos[1] = {buffer->bo};
		uint64_t da[1];
		int ret;

		ret = apu_bo_iommu_map(dev->drm, bos, da, 1);
		if (ret) {
			pthread_mutex_unlock(&buffer->lock);
			return 0;
		}

		buffer->da = (uint32_t) da[0];
	}
	pthread_mutex_unlock(&buffer->lock);

	return buffer->da;
}

uint32_t apu_iommu_unmap_buffer(struct apu_device *dev,
				struct apu_buffer *buffer)
{
	pthread_mutex_lock(&buffer->lock);
	if (buffer->iommu_refcount-- == 1) {
		struct apu_bo *bos[1] = {buffer->bo};
		int ret;

		ret = apu_bo_iommu_unmap(dev->drm, bos, 1);
		if (ret) {
			pthread_mutex_unlock(&buffer->lock);
			return ret;
		}

		buffer->da = 0;
	}
	pthread_mutex_unlock(&buffer->lock);

	return 0;
}
