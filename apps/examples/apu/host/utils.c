/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/time.h>

#include "sysfs.h"
#include <rich-iot/apu.h>
#include <rich-iot/utils.h>
#include <rich-iot/log.h>

#define TAG "libapu(sysfs.c)"

#define MAX_RPROC_DEVICES 16

/* map a APU device to a remoteproc id */
static int rproc_id_table[MAX_APU_DEVICES] = { -1 };

static void rproc_find_apu_devices(void) {
	int i, j;

	char *device_path;
	int ret;

	/* Already probed, exist */
	if (rproc_id_table[0] != -1)
		return;

	for (i = 0, j = 0; i < MAX_RPROC_DEVICES; i++) {
		device_path = get_rproc_device_path_quiet(i);
		if (!device_path)
			continue;

		ret = rproc_device_attr_read_cmp(device_path, "name", "apu");
		free(device_path);
		if (ret == 0)
			rproc_id_table[j++] = i;
	}
}

int device_to_rproc_id(int device_id) {
	rproc_find_apu_devices();

	if (device_id >= MAX_APU_DEVICES)
		return -EINVAL;

	if (rproc_id_table[device_id] == -1)
		return -ENODEV;

	return rproc_id_table[device_id];
}

static int rproc_write_wait_attr(int rproc_id, char *attrname, char *value,
				 char *expected)
{
	struct timeval timeout = { 1, 0 };
	char *device_path;
	int ret;

	device_path = get_rproc_device_path(rproc_id);
	if (!device_path)
		return -errno;

	ret = rproc_device_attr_write_wait(device_path, attrname, value,
					   expected, &timeout);
	free(device_path);

	return ret == -1 ? errno : 0;
}

static int rproc_read_cmp_attr(int rproc_id, char *attrname, char *expected)
{
	char *device_path;
	int ret;

	device_path = get_rproc_device_path(rproc_id);
	if (!device_path)
		return -errno;

	ret = rproc_device_attr_read_cmp(device_path, attrname, expected);
	free(device_path);

	if (ret == -1) {
		if (errno == EINVAL)
			return 0;
		else
			return -1;
	}

	return 1;
}

static int apu_is_offline(int device_id)
{
	int ret;
	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	ret = rproc_read_cmp_attr(rproc_id, "state", "offline");
	if (ret == 0)
		return -EBUSY;
	if (ret == -1)
		return -errno;
	return 1;
}

int apu_set_firmware_name(int device_id, char *name)
{
	int ret;
	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	ret = apu_is_offline(device_id);
	if (ret < 0) {
		ret = apu_stop(device_id);
		if (ret)
			return ret;
	}
	return rproc_write_wait_attr(rproc_id, "firmware", name, name);
}

int apu_start(int device_id)
{
	int ret;
	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	ret = apu_is_offline(device_id);
	if (ret < 0) {
		ret = apu_stop(device_id);
		if (ret)
			return ret;
	}
	ret = rproc_write_wait_attr(rproc_id, "state", "start", "running");
	if (ret)
		return ret;

	return 0;
}

int apu_stop(int device_id)
{
	int ret;
	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	ret = rproc_write_wait_attr(rproc_id, "state", "stop", "offline");
	if (ret)
		return ret;

	return 0;
}

int apu_restart(int device_id)
{
	int ret;

	/* The APU might be already stopped so just warn user */
	ret = apu_stop(device_id);
	if (ret)
		LOGE(TAG, "Failed to stop the APU\n");

	ret = apu_start(device_id);
	if (ret)
		LOGE(TAG, "Failed to restart the APU\n");

	return ret;
}

int apu_print_traces(int device_id, int trace_id)
{
	FILE *file;
	char fname[64];
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	snprintf(fname, 64, "/sys/kernel/debug/remoteproc/remoteproc%d/trace%d",
		 rproc_id, trace_id);

	file = fopen(fname, "r");
	if (file == NULL)
		return -1;

	LOGI(TAG, "\n** APU trace buffer **\n");
	while ((read = getline(&line, &len, file)) != -1) {
		LOGI(TAG, "%s", line);
	}
	LOGI(TAG, "\n**********************\n");

	if (line)
		free(line);

	fclose(file);

	return 0;
}

int apu_save_traces(int device_id, int trace_id, const char *log_file)
{
	FILE *file, *output;
	char fname[64];
	char * line = NULL;
	size_t len = 0;
	ssize_t read;

	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	snprintf(fname, 64, "/sys/kernel/debug/remoteproc/remoteproc%d/trace%d",
		 rproc_id, trace_id);

	file = fopen(fname, "r");
	if (file == NULL)
		return -1;

	output = fopen(log_file, "w");
	if (output == NULL) {
		fclose(file);
		return -1;
	}

	LOGI(TAG, "Saving APU log to %s\n", log_file);
	while ((read = getline(&line, &len, file)) != -1) {
		fprintf(output, "%s", line);
	}

	if (line)
		free(line);

	fclose(file);
	fclose(output);

	return 0;
}
