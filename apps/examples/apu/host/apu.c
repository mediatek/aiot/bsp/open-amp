/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <errno.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <poll.h>
#include <pthread.h>

#include <rich-iot/apu-host.h>
#include <rich-iot/memory.h>
#include <rich-iot/utils.h>
#include <rich-iot/log.h>
#include <uapi/mtk_apu.h>

#include <apu_drmif.h>

#define TAG "libapu(apu.c)"

struct sync_request_data {
	int result;
	pthread_cond_t *cv;
};

static void *apu_device_main_loop_th(void *arg);
static pthread_mutex_t mutex;

static void sync_request_callback(
	struct apu_inline_buffer *inline_buffer,
	struct apu_buffer **buffers, int count,
	int result, void *data)
{
	(void) inline_buffer;
	(void) buffers;
	(void) count;
	((struct sync_request_data *)data)->result = result;
	pthread_cond_t *cv = ((struct sync_request_data *)data)->cv;

	pthread_cond_signal(cv);
}

static void *apu_device_main_loop_th(void *arg)
{
	int ret;
	struct apu_device *dev = (struct apu_device *)arg;

	size_t size_out;
	void *data_out;

	while (!dev->stop) {
		struct apu_job *apu_job;
		struct apu_drm_job *job;

		job = apu_job_wait_any(dev->drm);
		if (!job)
			continue;

		apu_job = apu_job_get_data(job);

		data_out = apu_job->inline_buffer ? apu_job->inline_buffer->data_out : NULL;

		ret = apu_dequeue_result(job, &apu_job->result,
					data_out, &size_out);
		if (ret) {
			printf ("Failed to dequeue the request\n");
			continue;
		}

		if (apu_job->inline_buffer)
			apu_job->inline_buffer->size_out = size_out;

		if (!apu_job->callback)
			continue;

		apu_job->callback(apu_job->inline_buffer,
				  apu_job->buffers, apu_job->bo_count,
				  apu_job->result, apu_job->data);
	}

	pthread_exit(NULL);
}

int wait_for_apu(int device_id, struct timeval timeout)
{
	struct timeval start_time, end_time;
	struct apu_drm_device *drm;
	int rproc_id;
	int fd;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return rproc_id;

	fd = drmOpen("drm_apu", NULL);
	if (fd < 0) {
		LOGE(TAG, "Can't open dri card\n");
		return fd;
	}

	drm = apu_device_new(fd, device_id);
	if (!drm) {
		close(fd);
		LOGE(TAG, "Failed to create apu device\n");
		return -ENOMEM;
	}

	gettimeofday(&start_time, NULL);
	timeradd(&start_time, &timeout, &end_time);
	while (timercmp(&start_time, &end_time, <=)) {
		if (apu_device_online(drm)) {
			apu_device_del(drm);
			close(fd);
			return 0;
		}
		gettimeofday(&start_time, NULL);
	}

	return -ETIMEDOUT;
}

struct apu_device *apu_device_open(int device_id)
{
	struct apu_device *dev;
	int rproc_id;

	rproc_id = device_to_rproc_id(device_id);
	if (rproc_id < 0)
		return NULL;

	dev = malloc(sizeof(*dev));
	if (!dev)
		return NULL;

	dev->apu_fd = drmOpen("drm_apu", NULL);
	if (dev->apu_fd < 0) {
		LOGE(TAG, "Can't open dri card\n");
		return  NULL;
	}

	dev->drm = apu_device_new(dev->apu_fd, device_id);
	if (!dev->drm) {
		close(dev->apu_fd);
		LOGE(TAG, "Failed to create apu device\n");
		return NULL;
	}

	dev->stop = 0;

	pthread_mutex_init(&mutex, NULL);
	pthread_create(&dev->main_loop_thread, NULL, apu_device_main_loop_th, dev);

	return dev;
}

void apu_device_get(struct apu_device *dev) {
	apu_device_ref(dev->drm);
}

void apu_device_put(struct apu_device *dev) {
	if (!apu_device_del(dev->drm))
		return;

	dev->stop = 1;
	pthread_join(dev->main_loop_thread, NULL);
	close(dev->apu_fd);
	free(dev);
}

struct apu_inline_buffer *apu_inline_buffer(void *data_in, size_t size_in,
		size_t size_out, int flags)
{
	struct apu_inline_buffer *buffer;

	buffer = malloc(sizeof(*buffer));
	if (!buffer)
		return buffer;

	buffer->data_in = data_in;
	buffer->size_in = size_in;
	buffer->data_out = NULL;
	buffer->size_out = size_out;
	buffer->flags = flags;

	if (buffer->size_out) {
		buffer->data_out = malloc(buffer->size_out);
		if (!buffer->data_out) {
			free(buffer);
			return NULL;
		}
	}

	return buffer;
}

void apu_inline_buffer_free(struct apu_inline_buffer *buffer)
{
	if (buffer->data_out)
		free(buffer->data_out);
	free(buffer);
}

struct apu_inline_buffer *apu_inline_buffer_in(void *data_in, size_t size_in)
{
	return apu_inline_buffer(data_in, size_in, 0, 0);
}

struct apu_inline_buffer *apu_inline_buffer_rw(void *data_in, size_t size_in)
{
	return apu_inline_buffer(data_in, size_in, 0, INLINE_BUFFER_RW);
}

struct apu_inline_buffer *apu_inline_buffer_out(size_t size_out)
{
	return apu_inline_buffer(NULL, 0, size_out, 0);
}

size_t apu_inline_buffer_size(struct apu_inline_buffer *buffer)
{
	if (!buffer)
		return 0;

	return buffer->size_in + buffer->size_out;
}

void *apu_inline_buffer_in_read(struct apu_inline_buffer *buffer, size_t *len)
{
	if (buffer->flags & INLINE_BUFFER_RW) {
		if (len)
			*len = buffer->size_in;
		return buffer->data_in;
	}

	return NULL;
}

void *apu_inline_buffer_out_read(struct apu_inline_buffer *buffer, size_t *len)
{
	if (len)
		*len = buffer->size_out;
	return buffer->data_out;
}

size_t apu_request_size(struct apu_inline_buffer *buffer, int count)
{
	size_t size = sizeof(struct apu_request);

	size += apu_inline_buffer_size(buffer);
	size += sizeof(uint32_t) * count * 2;

	return size;
}

struct apu_request *apu_request_alloc(struct apu_inline_buffer *buffer,
				      int count)
{
	int size;
	struct apu_request *req;

	size = apu_request_size(buffer, count);
	req = malloc(size);
	if (!req)
		return NULL;

	if (buffer) {
		req->size_in = buffer->size_in;
		req->size_out = buffer->size_out;
		memcpy(req->data, buffer->data_in, buffer->size_in);
		buffer->data_in = NULL;
	} else {
		req->size_in = 0;
		req->size_out = 0;
	}

	req->count = count;

	return req;
}

int apu_request_update_inline_buffer(struct apu_request *req,
				     struct apu_inline_buffer *buffer)
{
	if (!buffer)
		return 0;

	if (buffer->flags & INLINE_BUFFER_RW && req->size_in) {
		buffer->size_in = req->size_in;
		buffer->data_in = malloc(req->size_in);
		if (!buffer->data_in)
			return -ENOMEM;
		memcpy(buffer->data_in, req->data, req->size_in);
	}

	if (req->size_out) {
		buffer->size_out = req->size_out;
		buffer->data_out = malloc(req->size_out);
		if (!buffer->data_out)
			return -ENOMEM;
		memcpy(buffer->data_out, req->data + req->size_in,
			 req->size_out);
	}

	return 0;
}

int _apu_exec(struct apu_device *dev, int cmd,
	      struct apu_inline_buffer *inline_buffer,
	      struct apu_buffer **buffers, int count,
	      apu_callback callback, void *data, struct apu_job **p_job)
{
	struct apu_job *apu_job;
	struct apu_drm_job *job;
	struct apu_bo **bos;
	size_t size_in = inline_buffer ? inline_buffer->size_in : 0;
	size_t size_out = inline_buffer ? inline_buffer->size_out : 0;
	void *data_in = inline_buffer ? inline_buffer->data_in : NULL;
	int ret;
	int i;

	job = apu_new_job(dev->drm, sizeof(*apu_job));
	if (!job) {
		return -ENOMEM;
	}

	apu_job = apu_job_get_data(job);
	apu_job->bos = malloc(sizeof(*bos) * count);
	if (!apu_job->bos) {
		apu_free_job(job);
		return -ENOMEM;
	}

	for (i = 0; i < count; i++)
		apu_job->bos[i] = buffers[i]->bo;
	apu_job->buffers = buffers;
	apu_job->inline_buffer = inline_buffer;
	apu_job->callback = callback;
	apu_job->data = data;

	ret = apu_job_init(job, cmd, apu_job->bos, count,
			   data_in, size_in, size_out);
	if (ret) {
		apu_free_job(job);
		return ret;
	}

	pthread_mutex_lock(&mutex);
	ret = apu_queue(job);
	if (ret) {
		printf ("Failed to queue a job\n");
		pthread_mutex_unlock(&mutex);
		apu_free_job(job);
		return ret;
	}
	pthread_mutex_unlock(&mutex);

	if (p_job)
		*p_job = apu_job;

	return 0;
}

int apu_vexec(struct apu_device *dev, int cmd,
	      struct apu_inline_buffer *inline_buffer,
	      int count, ...)
{
	struct apu_buffer *buffers[count];
	va_list ap;
	int i;

	va_start(ap, count);
	for (i = 0; i < count; i++)
		buffers[i] = va_arg(ap, struct apu_buffer *);
	va_end(ap);

	return apu_exec(dev, cmd, inline_buffer, buffers, count);
}

int apu_exec(struct apu_device *dev, int cmd,
		struct apu_inline_buffer *inline_buffer,
		struct apu_buffer **buffers, int count)
{
	struct sync_request_data data;
	pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
	pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
	int ret;

	data.result = -1;
	data.cv = &cv;
	ret = _apu_exec(dev, cmd, inline_buffer, buffers, count, sync_request_callback, &data, NULL);
	if (ret < 0)
		return ret;

	pthread_mutex_lock(&lock);
	pthread_cond_wait(&cv, &lock);
	pthread_mutex_unlock(&lock);
	return data.result;
}

int apu_exec_async(struct apu_device *dev, int cmd,
		struct apu_inline_buffer *inline_buffer,
		struct apu_buffer **buffers, int count,
		apu_callback callback, void *data)
{
	struct apu_job *job;
	int ret;

	ret = _apu_exec(dev, cmd, inline_buffer, buffers, count, callback, data, &job);
	if (ret)
		return ret;

	return 0;
}
