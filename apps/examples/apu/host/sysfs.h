/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __RICH_IOT_APU_SYSFS_H__
#define __RICH_IOT_APU_SYSFS_H__

char *get_rproc_device_path(int device_id);
char *get_rproc_device_path_quiet(int rproc_id);
int rproc_device_attr_write_wait(char *device_path, char *attr, char *value,
				 char *expected, struct timeval *timeout);
int rproc_device_attr_read_cmp(char *device_path, char *attr, char *expected);

#endif /* __RICH_IOT_APU_SYSFS_H__ */
