/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include <rich-iot/apu-host.h>
#include <rich-iot/apu-test.h>
#include <rich-iot/memory.h>
#include <rich-iot/utils.h>
#include <rich-iot/log.h>
#include <uapi/mtk_apu.h>

#define TAG "libapu(test.c)"

typedef int (*test_function)(struct apu_device *dev, void *args);

struct test {
	test_function fn;
	void *args;
	const char *name;
	const char *args_name;
};

#define TEST_FUNCTION(fn, args) \
	{ fn, args, #fn, #args }

#define INLINE_BUFFER_SIZE_MAX 256

static int device_id = 0;

#define test_failed(label)						\
	{								\
		LOGE(TAG, "%s failed at line %d\n", __func__, __LINE__);	\
		goto label;						\
	}

#define test_ended(label, err)							\
	{									\
		if (err)							\
			LOGE(TAG, "%s failed at line %d\n", __func__, __LINE__);	\
		goto label;							\
	}

static int _test_log_error(int err, const char *func, int line)
{
	if (err)
		LOGE(TAG, "%s returned %d at line %d\n", func, err, line);
	return err;
}
#define test_log_error(err) _test_log_error(err, __func__, __LINE__)

int apu_buffer_memset(struct apu_buffer *buffer, int value, size_t size,
		      int offset)
{
	void *ptr;

	ptr = apu_get_buffer(buffer);
	if (!ptr) {
		LOGE(TAG, "Failed to map memory\n");
		return -ENOMEM;
	}

	if (offset + size > buffer->size) {
		LOGE(TAG, "Invalid size or offset\n");
		apu_put_buffer(buffer);
		return -EINVAL;
	}

	memset(ptr + offset, value, size);
	apu_put_buffer(buffer);

	return 0;
}

struct apu_buffer *apu_buffer_alloc_init(struct apu_device *dev,
					 void *hostptr,
					 size_t buffer_size, int value,
					 size_t size, int offset)
{
	struct apu_buffer *buffer;

	if (!hostptr) {
		buffer = apu_alloc_buffer(dev, buffer_size);
		if (!buffer) {
			LOGE(TAG, "Failed to allocate buffer\n");
			return NULL;
		}
	} else {
		buffer = apu_alloc_user_buffer(dev, hostptr, buffer_size);
		if (!buffer) {
			LOGE(TAG, "Failed to allocate user buffer\n");
			return NULL;
		}
	}

	if (apu_buffer_memset(buffer, value, size, offset)) {
		apu_free_buffer(buffer);
		return NULL;
	}

	return buffer;
}

int apu_null(void *ptr, int len)
{
	uint8_t null_buffer[len];

	memset(null_buffer, 0, len);
	return !memcmp(null_buffer, ptr, len);
}

int _memcmp_faulty_regions(uint8_t *val1, uint8_t *val2, size_t start, size_t end)
{
	for (size_t i = start; i < end; i++) {
		if (val1[i] == val2[i])
			return i - start;
	}
	return end;
}

int memcmp_faulty_regions(void *ptr1, void *ptr2, size_t len)
{
	int ret;

	ret = memcmp(ptr1, ptr2, len);
	if (ret) {
		uint8_t *val1 = ptr1;
		uint8_t *val2 = ptr2;

		for (size_t i = 0; i < len; i++) {
			if (val1[i] != val2[i]) {
				int start = i;
				int end;

				end = start + _memcmp_faulty_regions(val1, val2, i, len - i);
				LOGE(TAG, "Expected: %x != %x from %d to %d\n",
					val1[i], val2[i], start, end);
				ret = -EINVAL;
				i = end;
			}
		}
	}

	return ret;
}

int apu_buffer_memcmp(struct apu_buffer *buffer1, struct apu_buffer *buffer2,
		      void *data)
{
	void *ptr1, *ptr2;
	int ret;

	ptr1 = apu_get_buffer(buffer1);
	if (!ptr1) {
		LOGE(TAG, "Failed to map buffer 1\n");
		return -ENOMEM;
	}

	if (!buffer2 && !data) {
		int len = buffer1->size / 2;
		ret = memcmp_faulty_regions(ptr1, ptr1 + len, len);
		ret |= apu_null(ptr1, len);
		test_ended(put_buf1, ret);
	}

	if (!buffer2 && data) {
		ret = memcmp_faulty_regions(ptr1, data, buffer1->size);
		ret |= apu_null(ptr1, buffer1->size);
		ret |= apu_null(data, buffer1->size);
		test_ended(put_buf1, ret);
	}

	if (buffer1->size != buffer2->size) {
		ret = -EINVAL;
		test_failed(put_buf1);
	}

	ptr2 = apu_get_buffer(buffer2);
	if (!ptr2) {
		LOGE(TAG, "Failed to map buffer 2\n");
		ret = -ENOMEM;
		test_failed(put_buf1);
	}

	ret = memcmp_faulty_regions(ptr1, ptr2, buffer1->size);
	ret |= apu_null(ptr1, buffer1->size);
	ret |= apu_null(ptr2, buffer1->size);

	apu_put_buffer(buffer2);
put_buf1:
	apu_put_buffer(buffer1);

	return test_log_error(ret);
}

int find_faulty_region(uint8_t *ptr, uint8_t value, size_t start, size_t end)
{
	for (size_t i = start; i < end; i++) {
		if (ptr[i] == value)
			return i - start;
	}
	return end;
}

int apu_buffer_memchk(struct apu_buffer *buffer, uint8_t value, size_t size)
{
	uint8_t *ptr;
	int ret = 0;

	ptr = apu_get_buffer(buffer);
	if (!ptr) {
		LOGE(TAG, "Failed to map buffer\n");
		ret = -ENOMEM;
		test_failed(put_buf);
	}

	if (!size)
		size = buffer->size;

	for (size_t i = 0; i < size; i++) {
		if (ptr[i] != value) {
			int start = i;
			int end;

			end = start + find_faulty_region(ptr, value, i, size - i);
			LOGE(TAG, "Expected: %x != %x from %d to %d\n",
				value, ptr[start], start, end);
			ret = -EINVAL;
			i = end;
		}
	}

put_buf:
	apu_put_buffer(buffer);

	return test_log_error(ret);
}

struct test_basic_args {
	int count;
	int size;
};

static int test_copy_hostptr_buffer(struct apu_device *dev, void *args)
{

	struct test_basic_args *p_args = args;
	struct apu_buffer *buffer1, *buffer2;
	uint8_t val1, val2;
	int ret;
	int i;

	if (!p_args)
		return test_log_error(-EINVAL);

	for (i = 0; i < p_args->count; i++) {
		void *hostptr1 = (i & 1) ? malloc(p_args->size) : NULL;
		void *hostptr2 = (i & 2) ? malloc(p_args->size) : NULL;

		val1 = (rand() % 254) + 1;
		val2 = (rand() % 254) + 1;
		buffer1 = apu_buffer_alloc_init(dev, hostptr1, p_args->size, val1,
						p_args->size, 0);
		if (!buffer1)
			return test_log_error(-ENOMEM);

		buffer2 = apu_buffer_alloc_init(dev, hostptr2, p_args->size, val2,
						p_args->size, 0);
		if (!buffer2)
			test_failed(err_free_buf1);

		ret = apu_vexec(dev, TEST_COPY_SHARED_BUFFER, NULL, 2, buffer1, buffer2);
		if (ret)
			test_failed(err_free_buf2);

		ret = apu_buffer_memcmp(buffer1, buffer2, NULL);
		if (ret)
			test_failed(err_free_buf2);

		ret = apu_buffer_memchk(buffer1, val1, 0);
		if (ret)
			test_failed(err_free_buf2);

		ret = apu_buffer_memchk(buffer2, val1, 0);
		if (ret)
			test_failed(err_free_buf2);

		apu_free_buffer(buffer1);
		apu_free_buffer(buffer2);
		free(hostptr1);
		free(hostptr2);
	}

	return 0;

err_free_buf2:
	apu_free_buffer(buffer2);
err_free_buf1:
	apu_free_buffer(buffer1);

	return test_log_error(ret);
}

static int test_copy_shared_buffer(struct apu_device *dev, void *args)
{
	struct test_basic_args *p_args = args;
	struct apu_buffer *buffer1, *buffer2;
	int ret;
	int i;

	if (!p_args)
		return test_log_error(-EINVAL);

	for (i = 0; i < p_args->count; i++) {
		buffer1 = apu_buffer_alloc_init(dev, NULL, p_args->size, 0x12,
						p_args->size / 2, 0);
		if (!buffer1)
			return test_log_error(-ENOMEM);

		ret = apu_vexec(dev, TEST_COPY_SHARED_BUFFER, NULL, 1, buffer1);
		if (ret)
			test_failed(err_free_buf1);

		ret = apu_buffer_memcmp(buffer1, NULL, NULL);
		if (ret)
			test_failed(err_free_buf1);

		buffer2 = apu_buffer_alloc_init(dev, NULL, p_args->size, 0x14,
						p_args->size, 0);
		if (!buffer2) {
			ret = -ENOMEM;
			test_failed(err_free_buf1);
		}

		ret = apu_vexec(dev, TEST_COPY_SHARED_BUFFER, NULL,
				2, buffer1, buffer2);
		if (ret)
			test_failed(err_free_buf2);;

		ret = apu_buffer_memcmp(buffer1, buffer2, NULL);
		if (ret)
			test_failed(err_free_buf2);;

		ret = apu_buffer_memchk(buffer1, 0x12, p_args->size / 2);
		if (ret)
			test_failed(err_free_buf2);

		ret = apu_buffer_memchk(buffer2, 0x12, p_args->size / 2);
		if (ret)
			test_failed(err_free_buf2);

		apu_free_buffer(buffer2);
		apu_free_buffer(buffer1);
	}

	return 0;

err_free_buf2:
	apu_free_buffer(buffer2);
err_free_buf1:
	apu_free_buffer(buffer1);

	return test_log_error(ret);
}

static int test_copy_inline_buffer(struct apu_device *dev, void *args)
{
	(void)args;
	int i;
	int ret;
	char data_in[INLINE_BUFFER_SIZE_MAX];
	char *data_out;
	struct apu_inline_buffer *inline_buffer;

	for (i = 1; i < INLINE_BUFFER_SIZE_MAX / 2; i <<= 1) {
		inline_buffer = apu_inline_buffer(data_in, i, i, 0);
		if (!inline_buffer)
			return test_log_error(-ENOMEM);

		memset(data_in, i, i);
		ret = apu_vexec(dev, TEST_COPY_INLINE_BUFFER, inline_buffer, 0);
		if (ret) {
			apu_inline_buffer_free(inline_buffer);
			return test_log_error(ret);
		}

		data_out = apu_inline_buffer_out_read(inline_buffer, NULL);
		if (memcmp(data_in, data_out, i)) {
			apu_inline_buffer_free(inline_buffer);
			return test_log_error(-EINVAL);
		}
		apu_inline_buffer_free(inline_buffer);
	}

	return 0;
}

static int test_copy_inline_to_shared_buffer(struct apu_device *dev, void *args)
{
	(void)args;
	struct apu_inline_buffer *inline_buffer;
	char data[INLINE_BUFFER_SIZE_MAX];
	struct apu_buffer *buffer;
	int ret;

	inline_buffer = apu_inline_buffer_in(data, INLINE_BUFFER_SIZE_MAX);
	if (!inline_buffer)
		return test_log_error(-ENOMEM);

	buffer = apu_buffer_alloc_init(dev, NULL, INLINE_BUFFER_SIZE_MAX, 0x12,
				       INLINE_BUFFER_SIZE_MAX, 0);
	if (!buffer) {
		ret = -ENOMEM;
		test_failed(err_free_inline_buf);
	}

	memset(data, 0x65, INLINE_BUFFER_SIZE_MAX);
	ret = apu_vexec(dev, TEST_COPY_INLINE2SHARED, inline_buffer, 1, buffer);
	if (ret)
		test_failed(err_free_buf);

	ret = apu_buffer_memcmp(buffer, NULL, data);

err_free_buf:
	apu_free_buffer(buffer);
err_free_inline_buf:
	apu_inline_buffer_free(inline_buffer);

	return test_log_error(ret);
}

static int test_copy_shared_to_inline_buffer(struct apu_device *dev, void *args)
{
	(void)args;
	struct apu_inline_buffer *inline_buffer;
	struct apu_buffer *buffer;
	void *data;
	int ret;

	inline_buffer = apu_inline_buffer_out(INLINE_BUFFER_SIZE_MAX);
	if (!inline_buffer)
		return test_log_error(-ENOMEM);

	buffer = apu_buffer_alloc_init(dev, NULL, INLINE_BUFFER_SIZE_MAX, 0x12,
				       INLINE_BUFFER_SIZE_MAX, 0);
	if (!buffer) {
		ret = -ENOMEM;
		test_failed(err_free_inline_buf);
	}

	ret = apu_vexec(dev, TEST_COPY_SHARED2INLINE, inline_buffer, 1, buffer);
	if (ret)
		test_failed(err_free_buf);

	data = apu_inline_buffer_out_read(inline_buffer, NULL);
	ret = apu_buffer_memcmp(buffer, NULL, data);

err_free_buf:
	apu_free_buffer(buffer);
err_free_inline_buf:
	apu_inline_buffer_free(inline_buffer);

	return test_log_error(ret);
}

static int test_fill_buffer(struct apu_device *dev, void *args)
{
	(void)args;
	struct apu_inline_buffer *inline_buffer;
	char data_in[INLINE_BUFFER_SIZE_MAX];
	char size_in = sizeof(uint8_t) + sizeof(uint16_t);
	char *data_out;
	char expected_data[INLINE_BUFFER_SIZE_MAX];
	struct apu_buffer *buffer;
	size_t len;
	int ret;

	memset(expected_data, 0x56, INLINE_BUFFER_SIZE_MAX);

	inline_buffer = apu_inline_buffer(data_in, size_in,
					  INLINE_BUFFER_SIZE_MAX, 0);
	if (!inline_buffer)
		return test_log_error(-ENOMEM);

	buffer = apu_buffer_alloc_init(dev, NULL, INLINE_BUFFER_SIZE_MAX, 0x12,
				       INLINE_BUFFER_SIZE_MAX, 0);
	if (!buffer) {
		ret = -ENOMEM;
		test_failed(err_free_inline_buf);
	}

	data_in[0] = 0x56;
	*((uint16_t *)&data_in[1]) = 120;
	ret = apu_vexec(dev, TEST_FILL_BUFFER, inline_buffer, 0);
	if (ret)
		test_failed(err_free_buf);

	data_out = apu_inline_buffer_out_read(inline_buffer, &len);
	if (len != 120) {
		ret = -EINVAL;
		test_failed(err_free_buf);
	}

	ret = memcmp(data_out, expected_data, len);
	if (ret)
		test_failed(err_free_buf);

err_free_buf:
	apu_free_buffer(buffer);
err_free_inline_buf:
	apu_inline_buffer_free(inline_buffer);

	return test_log_error(ret);
}

static int test_buffer_iommu_mmap(struct apu_device *dev, void *args)
{
	(void)args;
	struct apu_inline_buffer *inline_buffer;
	struct test_buffer_iommu_mmap test_buffer;
	struct apu_buffer *buffer1, *buffer2;
	uint32_t buffer1_da, buffer2_da;
	int ret;

	buffer1 = apu_buffer_alloc_init(dev, NULL, INLINE_BUFFER_SIZE_MAX, 0x12,
					INLINE_BUFFER_SIZE_MAX, 0);
	if (!buffer1)
		return test_log_error(-ENOMEM);

	buffer1_da = apu_iommu_map_buffer(dev, buffer1);
	if (!buffer1_da) {
		ret = -ENOMEM;
		test_failed(err_free_buffer1);
	}

	buffer2 = apu_buffer_alloc_init(dev, NULL, INLINE_BUFFER_SIZE_MAX, 0x15,
					INLINE_BUFFER_SIZE_MAX, 0);
	if (!buffer2) {
		ret = -ENOMEM;
		test_failed(err_unmap_buffer1);
	}

	buffer2_da = apu_iommu_map_buffer(dev, buffer2);
	if (!buffer2_da) {
		ret = -ENOMEM;
		test_failed(err_free_buffer2);
	}

	test_buffer.buffer_in_da = buffer1_da;
	test_buffer.buffer_out_da = buffer2_da;
	test_buffer.size = INLINE_BUFFER_SIZE_MAX;
	inline_buffer = apu_inline_buffer_in(&test_buffer, sizeof(test_buffer));
	if (!inline_buffer) {
		ret = -ENOMEM;
		test_failed(err_unmap_buffer2);
	}

	ret = apu_vexec(dev, TEST_BUFFER_IOMMU_MMAP, inline_buffer, 0);
	if (ret)
		test_failed(err_free_inline_buf);

	ret = apu_buffer_memcmp(buffer1, buffer2, NULL);

err_free_inline_buf:
	apu_inline_buffer_free(inline_buffer);
err_unmap_buffer2:
	apu_iommu_unmap_buffer(dev, buffer2);
err_free_buffer2:
	apu_free_buffer(buffer2);
err_unmap_buffer1:
	apu_iommu_unmap_buffer(dev, buffer1);
err_free_buffer1:
	apu_free_buffer(buffer1);

	return test_log_error(ret);
}

static int test_timeout(struct apu_device *dev, void *args)
{
	struct timeval timeout = { 1, 0 };
	int ret;

	ret = apu_vexec(dev, TEST_TIMEOUT, NULL, 0);
	if (ret != ETIMEDOUT)
		return test_log_error(ret);

	/* This test makes the APU hangs: restart the APU */
	ret = apu_restart(device_id);
	if (ret) {
		LOGE(TAG, "Failed to start core %d\n", device_id);
		return test_log_error(ret);
	}

	ret = wait_for_apu(device_id, timeout);
	if (ret)
		return test_log_error(ret);

	return 0;
}

static int test_offline(struct apu_device *dev, void *args)
{
	int ret;

	ret = apu_vexec(dev, TEST_TIMEOUT, NULL, 0);
	if (ret == -ENODEV)
		return 0;

	return -1;
}

struct test_basic_args test_basic_1_256 = {1, 256};
struct test_basic_args test_basic_1_4096 = {1, 4096};
struct test_basic_args test_basic_1_65535 = {1, 65535};

struct test_basic_args test_basic_4096_256 = {4096, 256};
struct test_basic_args test_basic_4096_4096 = {4096, 4096};
struct test_basic_args test_basic_4096_65535 = {4096, 65535};

struct test tests[] = {
	TEST_FUNCTION(test_copy_hostptr_buffer, &test_basic_1_4096),
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_1_256),
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_1_4096),
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_1_65535),
	TEST_FUNCTION(test_copy_inline_buffer, &test_basic_1_256),
	TEST_FUNCTION(test_copy_inline_to_shared_buffer, NULL),
	TEST_FUNCTION(test_copy_shared_to_inline_buffer, NULL),
	TEST_FUNCTION(test_fill_buffer, NULL),
	TEST_FUNCTION(test_buffer_iommu_mmap, NULL),
	TEST_FUNCTION(test_timeout, NULL),
};

struct test long_run_tests[] = {
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_4096_256),
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_4096_4096),
	TEST_FUNCTION(test_copy_shared_buffer, &test_basic_4096_65535),
};

struct test apu_offline_tests[] = {
	TEST_FUNCTION(test_offline, NULL),
};

int run_tests(struct apu_device *dev, struct test *tests, int count)
{
	int i;
	int ret;
	int test_failed = 0;


	for (i = 0; i < count; i++) {
		ret = tests[i].fn(dev, tests[i].args);
		if (ret)
			test_failed++;
		LOGI(TAG, "%s - %s, %s\n", tests[i].name, tests[i].args_name,
		       ret ? "failed" : "passed");
	}

	return test_failed;
}

void callback(
		struct apu_inline_buffer *inline_buffer,
		struct apu_buffer **buffers, int count,
		int result, void *data)
{
	(void) inline_buffer;
	(void) count;
	int ret;

	if (result)
		goto free_request;

	ret = apu_buffer_memcmp(*buffers, NULL, NULL);
	if (ret)
		LOGI(TAG, "error\n");
	else
		LOGI(TAG, "passed!\n");
free_request:
	apu_free_buffer(buffers[0]);
	free(buffers);
}

int async_test(struct apu_device *dev)
{
	struct apu_buffer **buffers = malloc(sizeof(*buffers) * 1);
	struct apu_buffer *buffer1;
	int ret;

	buffer1 = apu_buffer_alloc_init(dev, NULL, 256, 0x12, 128, 0);
	if (!buffer1)
		return test_log_error(-ENOMEM);
	buffers[0] = buffer1;
	ret = apu_exec_async(dev, TEST_COPY_SHARED_BUFFER, NULL, buffers, 1, callback, dev);
	apu_device_put(dev);
	return ret;
}

/* Make sure that remoteproc is always stopped, even in case of error */
void cleanup_rproc(void)
{
	apu_stop(device_id);
}

/* Main - Call the ioctl functions */
int main(int argc, char *argv[])
{
	struct timeval timeout = { 1, 0 };
	struct apu_device *dev;
	char fwname[32];
	int total_test_count = 0;
	int test_count;
	int test_failed = 0;
	int run_long_test = 0;
	int opt;
	int ret;

	while ((opt = getopt(argc, argv, "ld:")) != -1) {
		switch (opt) {
		case 'l':
			run_long_test = 1;
			break;
		case 'd':
			device_id = atoi(optarg);
			break;
		default:
			LOGE(TAG, "Usage: %s [-d device id] [-l]\n"
				"\t-d device_id: select the APU to use to run the tests\n"
				"\t-l: do more intensive and long test\n",
				argv[0]);
			return -EINVAL;
		}
	}

	/*
	 * For testing purpose, we open the device before it have been started
	 * which not really recommended.
	 */
	dev = apu_device_open(device_id);
	if (!dev)
		return -ENODEV;

	test_count = sizeof(apu_offline_tests) / sizeof(struct test);
	total_test_count += test_count;
	test_failed += run_tests(dev, apu_offline_tests, test_count);

	snprintf(fwname, 32, "rity_nn_test_apu%d", device_id);
	ret = apu_set_firmware_name(device_id, fwname);
	if (ret) {
		LOGE(TAG, "Failed to select firmware to load for device %d\n",
			device_id);
		return ret;
	}

	ret = apu_start(device_id);
	if (ret) {
		LOGE(TAG, "Failed to start core %d\n", device_id);
		return ret;
	}

	atexit(cleanup_rproc);

	ret = wait_for_apu(device_id, timeout);
	if (ret)
		return ret;

	test_count = sizeof(tests) / sizeof(struct test);
	total_test_count += test_count;
	test_failed += run_tests(dev, tests, test_count);

	if (run_long_test) {
		test_count = sizeof(long_run_tests) / sizeof(struct test);
		total_test_count += test_count;
		test_failed += run_tests(dev, long_run_tests, test_count);
	}

	LOGI(TAG, "%d / %d passed\n", total_test_count - test_failed,
		total_test_count);

	apu_device_get(dev);
	async_test(dev);
	apu_device_put(dev);

	return 0;
}
