#ifndef ANDROID_ML_XRP_ION_H
#define ANDROID_ML_XRP_ION_H

#include <uapi/ion.h>

int find_ion_heap_id(int ion_client, enum ion_heap_type type);

#endif /* ANDROID_ML_XRP_ION_H */
