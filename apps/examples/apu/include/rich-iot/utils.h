/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __RICH_IOT_APU_UTILS_H__
#define __RICH_IOT_APU_UTILS_H__

#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Set the name of the firmware to load
 * By default remoteproc will try to load a firmware named rproc-apu-fw.
 * As the APU is intended to load and execute many different firmwares,
 * add a function to set the name of the firmware we want to load.
 * This must be used while the APU is offline, before to call apu_start().
 * @arg device_id the id of the APU to configure
 * @arg name the firmware name
 * @return 0 on success, and a negative number in the case of error
 */
int apu_set_firmware_name(int device_id, char *name);

/**
 * @brief Start the APU
 * This instructs remoteproc to load the firmware, and to run it.
 * On success, this should create /dev/apuX (where X is the device_id).
 * Still, there is no guaranty that the file will created on time, so you shall
 * use wait_for_apu() before to try to open /dev/apuX.
 * @arg device the id of the APU to start
 * @return 0 on success, and a negative number in the case of error
 */
int apu_start(int device_id);

/**
 * @brief Stop the APU
 * This instructs remoteproc to stop and unload the firmware.
 * On success, this removes /dev/apuX and release all the resources allocated
 * by remoteproc.
 * This function will stop remoteproc only if it has been preceded by a call
 * to apu_start(). This prevents to stop by misstake a firmware ran by another
 * software.
 * @arg device the id of the APU to stop
 * @return 0 on success, and a negative number in the case of error
 */
int apu_stop(int device_id);

/**
 * @brief Restart the APU
 * This instructs remoteproc to reload the firmware, and to run it.
 * On success, this should create /dev/apuX (where X is the device_id).
 * Still, there is no guaranty that the file will created on time, so you shall
 * use wait_for_apu() before to try to open /dev/apuX.
 * @arg device the id of the APU to restart
 * @return 0 on success, and a negative number in the case of error
 */
int apu_restart(int device_id);

/**
 * @brief Wait until /dev/apuX has been created
 * When apu_start() is called, it start the firmwares.
 * This could take an undetermined time to complete before the kernel get
 * a notification from the firmware and create /dev/apuX.
 * This functions could use to make sure that the device has been created
 * before to continue.
 * @arg device_id the id of the APU
 * @arg timeout a timeval struct to configure how long to wait before to time
 *              out
 * @return 0 on success, and a negative number in the case of error
 */
int wait_for_apu(int device_id, struct timeval timeout);

/**
 * @brief Dump a trace file
 * Currently, the APU uses a trace buffer as output of print functions.
 * The trace buffer is destroyed when remoteproc is stopped.
 * This functions read the trace buffer and print its content to stdout.
 * This must be called before to call apu_stop().
 * @arg device_id the id of the APU
 * @arg trace_id the id of the trace buffer, usually 0
 * @return 0 on success, and a negative number in the case of error
 */
int apu_print_traces(int device_id, int trace_id);

/**
 * @brief Save a trace file
 * Currently, the APU uses a trace buffer as output of print functions.
 * The trace buffer is destroyed when remoteproc is stopped.
 * This functions read the trace buffer and save its content to log_file.
 * This must be called before to call apu_stop().
 * @arg device_id the id of the APU
 * @arg trace_id the id of the trace buffer, usually 0
 * @arg log_file path to file to create and use to save the traces
 * @return 0 on success, and a negative number in the case of error
 */
int apu_save_traces(int device_id, int trace_id, const char *log_file);

/**
 * @brief Convert a device id to a remoteproc id
 * The kernel may create remoteproc device that are not APU devices.
 * This may add an offset and the APU 0 may be remoteproc device 1.
 * This function go through remoteproc devices and find the APU devices, then
 * return the remoteproc device id corresponding to the device id.
 * @arg device_id the id of the APU
 * @return the id of remoteproc device to use
 */
#ifndef __ANDROID__
int device_to_rproc_id(int device_id);
#else
static inline int device_to_rproc_id(int device_id) { return device_id; }
#endif

#ifdef __cplusplus
}
#endif

#endif /* __RICH_IOT_APU_UTILS_H__ */
