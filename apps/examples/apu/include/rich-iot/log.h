/*
 * Copyright (C) 2020 BayLibre SAS
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __RICH_IOT_LOG_H__
#define __RICH_IOT_LOG_H__

#ifdef __ANDROID__
#include <android/log.h>
#define LOGD(tag, ...) __android_log_print(ANDROID_LOG_DEBUG, tag, __VA_ARGS__)
#define LOGI(tag, ...) __android_log_print(ANDROID_LOG_INFO, tag, __VA_ARGS__)
#define LOGW(tag, ...) __android_log_print(ANDROID_LOG_WARN, tag, __VA_ARGS__)
#define LOGE(tag, ...) __android_log_print(ANDROID_LOG_ERROR, tag, __VA_ARGS__)
#else
#include <stdio.h>
#define LOGD(tag, ...) do { \
	printf("D %s: ", tag); \
	printf(__VA_ARGS__); \
	printf("\n"); \
} while (0)

#define LOGI(tag, ...) do { \
	printf("I %s: ", tag); \
	printf(__VA_ARGS__); \
	printf("\n"); \
} while (0)

#define LOGW(tag, ...) do { \
	printf("W %s: ", tag); \
	printf(__VA_ARGS__); \
	printf("\n"); \
} while (0)

#define LOGE(tag, ...) do { \
	printf("E %s: ", tag); \
	printf(__VA_ARGS__); \
	printf("\n"); \
} while (0)
#endif

#endif /* __RICH_IOT_LOG_H__ */
